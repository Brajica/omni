export class CarsStateModel {
    public items: Car[] = [];
    loading: boolean = false;
}

export interface Car {
    id?: string;
    photo: string;
    model: string;
    year: string;
    brand: string;
    price: number;
}
