import { Injectable } from '@angular/core';
import { delay, map, Observable } from 'rxjs';
import { Car } from './cars.model';
import { HttpClient } from '@angular/common/http';
// import * as jsonURL from '';
@Injectable({
    providedIn: 'root',
})
export class CarsService {
    constructor(private http: HttpClient) {}

    /**
     * @description send request to get all cars
     */
    get(): Observable<Car[]> {
        return this.http.get('./assets/db.json').pipe(
            delay(2000),
            map(({ cars }: any) => {
                return cars as Car[];
            })
        );
    }
}
