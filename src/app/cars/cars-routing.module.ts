import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CarsComponent} from "./cars.component";
import {CarsDetailComponent} from "./cars-detail/cars-detail.component";

const routes: Routes = [{
  path: '',
  component: CarsComponent
}, {
  path: 'detail/:id',
  component: CarsDetailComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarsRoutingModule { }
