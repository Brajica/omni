import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Select } from '@ngxs/store';
import { CarsState } from '../../state/cars/cars.state';
import { Car, CarsStateModel } from '../../state/cars/cars.model';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-cars-detail',
    templateUrl: './cars-detail.component.html',
    styleUrls: ['./cars-detail.component.scss'],
})
export class CarsDetailComponent implements OnInit, OnDestroy {
    subscriptionCar: Subscription;
    // @ts-ignore
    @Select(CarsState) cars$: Observable<CarsStateModel>;
    car: Partial<Car> = {};

    constructor(private route: ActivatedRoute) {}

    ngOnInit(): void {
        const id = this.route.snapshot.paramMap.get('id');
        this.subscriptionCar = this.cars$.subscribe((carState) => {
            this.car = carState.items.find(
                (item) => item.id === id
            ) as Partial<Car>;
        });
    }

    ngOnDestroy() {
        this.subscriptionCar.unsubscribe();
    }
}
