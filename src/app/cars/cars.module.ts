import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarsRoutingModule } from './cars-routing.module';
import { CarsComponent } from './cars.component';
import { CarListComponent } from './car-list/car-list.component';
import { DataViewModule } from 'primeng/dataview';
import { InputTextModule } from 'primeng/inputtext';
import { CarsDetailComponent } from './cars-detail/cars-detail.component';
import { ButtonModule } from 'primeng/button';
import { RatingModule } from 'primeng/rating';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [CarsComponent, CarListComponent, CarsDetailComponent],
    imports: [
        CommonModule,
        CarsRoutingModule,
        DataViewModule,
        InputTextModule,
        ButtonModule,
        RatingModule,
        FormsModule,
    ],
})
export class CarsModule {}
