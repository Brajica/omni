import { Component, OnDestroy, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { CarsStateModel } from '../state/cars/cars.model';
import { CarsState } from '../state/cars/cars.state';

@Component({
    selector: 'app-cars',
    templateUrl: './cars.component.html',
    styleUrls: ['./cars.component.scss'],
})
export class CarsComponent implements OnInit, OnDestroy {
    subscriptionCar: Subscription;
    // @ts-ignore
    @Select(CarsState) cars$: Observable<CarsStateModel>;
    carsState: Partial<CarsStateModel> = {};

    constructor() {}

    ngOnInit(): void {
        this.subscriptionCar = this.cars$.subscribe((carState) => {
            this.carsState = carState;
        });
    }

    ngOnDestroy() {
        this.subscriptionCar.unsubscribe();
    }
}
