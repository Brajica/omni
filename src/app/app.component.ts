import { Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { FetchCars } from './state/cars/cars.actions';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    constructor(private store: Store) {
        this.store.dispatch(new FetchCars());
    }

    title = 'omni';
}
